/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Referimiento;

import Inventario.Inventario;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author OsLo
 */
public class ModificarReferimiento extends javax.swing.JPanel {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("ClinicasPU");
    ReferimientoJpaController ref = new ReferimientoJpaController(emf);
    DefaultTableModel model;
    private TableRowSorter trsFiltro;
    int idMedico = 0;
    /**
     * Creates new form ModificarReferimiento
     */
    public ModificarReferimiento() {
        initComponents();
        CrearModelo();
        CargarDatos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelBusqueda = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        VisualizarReferimiento = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        CategoriaBusqueda = new javax.swing.JComboBox<>();
        txtBusqueda = new javax.swing.JTextField();
        PanelIngreso = new javax.swing.JPanel();
        btnModificar = new javax.swing.JButton();
        jNombre = new javax.swing.JLabel();
        nMedico = new javax.swing.JTextField();
        jApellido = new javax.swing.JLabel();
        aMedico = new javax.swing.JTextField();
        jTelefono = new javax.swing.JLabel();
        Telefono = new javax.swing.JTextField();
        nClinica = new javax.swing.JTextField();
        jApellido1 = new javax.swing.JLabel();
        jApellido2 = new javax.swing.JLabel();
        Direccion = new javax.swing.JTextField();
        jApellido3 = new javax.swing.JLabel();
        especializacion = new javax.swing.JTextField();

        PanelBusqueda.setBorder(javax.swing.BorderFactory.createTitledBorder("Busqueda"));

        VisualizarReferimiento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        VisualizarReferimiento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                VisualizarReferimientoMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(VisualizarReferimiento);

        jLabel1.setText("Busqueda por: ");

        CategoriaBusqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Apellido", "Clinica", "Telefono" }));
        CategoriaBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CategoriaBusquedaActionPerformed(evt);
            }
        });

        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout PanelBusquedaLayout = new javax.swing.GroupLayout(PanelBusqueda);
        PanelBusqueda.setLayout(PanelBusquedaLayout);
        PanelBusquedaLayout.setHorizontalGroup(
            PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBusquedaLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 838, Short.MAX_VALUE)
                    .addGroup(PanelBusquedaLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(14, 14, 14)
                        .addComponent(CategoriaBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(PanelBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelBusquedaLayout.setVerticalGroup(
            PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBusquedaLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addGroup(PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelBusquedaLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel1))
                    .addComponent(CategoriaBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        PanelIngreso.setBorder(javax.swing.BorderFactory.createTitledBorder("Ingreso"));

        btnModificar.setText("Guardar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        jNombre.setText("Nombre(s): ");

        jApellido.setText("Apellido(s):");

        jTelefono.setText("Telefono: ");

        jApellido1.setText("Clínica: ");

        jApellido2.setText("Dirección:");

        jApellido3.setText("Especialización:");

        javax.swing.GroupLayout PanelIngresoLayout = new javax.swing.GroupLayout(PanelIngreso);
        PanelIngreso.setLayout(PanelIngresoLayout);
        PanelIngresoLayout.setHorizontalGroup(
            PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelIngresoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnModificar)
                    .addGroup(PanelIngresoLayout.createSequentialGroup()
                        .addComponent(jNombre)
                        .addGap(75, 75, 75)
                        .addComponent(nMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelIngresoLayout.createSequentialGroup()
                        .addComponent(jApellido)
                        .addGap(78, 78, 78)
                        .addComponent(aMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelIngresoLayout.createSequentialGroup()
                        .addComponent(jApellido3)
                        .addGap(50, 50, 50)
                        .addComponent(especializacion, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelIngresoLayout.createSequentialGroup()
                        .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTelefono)
                            .addComponent(jApellido1)
                            .addComponent(jApellido2))
                        .addGap(84, 84, 84)
                        .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nClinica, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Direccion, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Telefono, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(34, Short.MAX_VALUE))
        );
        PanelIngresoLayout.setVerticalGroup(
            PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelIngresoLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jNombre)
                    .addComponent(nMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(76, 76, 76)
                .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jApellido)
                    .addComponent(aMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(especializacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jApellido3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 72, Short.MAX_VALUE)
                .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jApellido1)
                    .addComponent(nClinica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(61, 61, 61)
                .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jApellido2)
                    .addComponent(Direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(78, 78, 78)
                .addGroup(PanelIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTelefono)
                    .addComponent(Telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addComponent(btnModificar)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PanelBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(PanelIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PanelBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PanelIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(64, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void VisualizarReferimientoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_VisualizarReferimientoMouseClicked
        EntityManager em = emf.createEntityManager();
        int row = VisualizarReferimiento.rowAtPoint(evt.getPoint());
        nMedico.setText(VisualizarReferimiento.getValueAt(row, 0).toString());
        aMedico.setText(VisualizarReferimiento.getValueAt(row, 1).toString());
        especializacion.setText(VisualizarReferimiento.getValueAt(row, 2).toString());
        nClinica.setText(VisualizarReferimiento.getValueAt(row, 3).toString());
        Telefono.setText(VisualizarReferimiento.getValueAt(row, 4).toString());
        Direccion.setText(VisualizarReferimiento.getValueAt(row, 5).toString());
        Query query = em.createNamedQuery("Referimiento.findId", Referimiento.class);
        query.setParameter("nombresMedico",nMedico.getText());
        query.setParameter("apellidosMedico",aMedico.getText());
        query.setParameter("especializacion",especializacion.getText());
        query.setParameter("nombreClinica",nClinica.getText());
        query.setParameter("telefonoClinica",Telefono.getText());
        query.setParameter("direccionClinica",Direccion.getText());
        List<Referimiento> list = query.getResultList( );
            for (Referimiento r : list) {
                idMedico = r.getId();
            }
                System.out.println(idMedico);
    }//GEN-LAST:event_VisualizarReferimientoMouseClicked

    private void CategoriaBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CategoriaBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CategoriaBusquedaActionPerformed
    public void ClearTable() {
        for (int i = 0; i < VisualizarReferimiento.getRowCount(); i++) {
            model.removeRow(i);
            i -= 1;
        }
    }
    private void txtBusquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyTyped
        // TODO add your handling code here:
        txtBusqueda.addKeyListener(new KeyAdapter(){
            public void keyReleased(final KeyEvent e) {
                String cadena = (txtBusqueda.getText());
                txtBusqueda.setText(cadena);
                repaint();
                Buscar();
            }
        });
        trsFiltro = new TableRowSorter(VisualizarReferimiento.getModel());
        VisualizarReferimiento.setRowSorter(trsFiltro);
    }//GEN-LAST:event_txtBusquedaKeyTyped

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        EntityManager em = emf.createEntityManager();
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Esta seguro de guardar?", "Guardar", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (respuesta == 0) {
            em.getTransaction().begin();
            Query query = em.createNamedQuery("Referimiento.Update", Referimiento.class);
            query.setParameter("nombresMedico",nMedico.getText());
            query.setParameter("apellidosMedico",aMedico.getText());        
            query.setParameter("especializacion",especializacion.getText());
            query.setParameter("nombreClinica",nClinica.getText());
            query.setParameter("telefonoClinica",Telefono.getText());
            query.setParameter("direccionClinica",Direccion.getText());
            query.setParameter("id",idMedico);
            query.executeUpdate();
            em.getTransaction().commit();
            em.close();
            JOptionPane.showConfirmDialog(null, "¡El médico referido ha sido modificado correctamente!", "¡EXITO!", JOptionPane.DEFAULT_OPTION, JOptionPane.DEFAULT_OPTION);
            CargarDatos();
            nMedico.setText("");
            aMedico.setText("");
            especializacion.setText("");
            nClinica.setText("");
            Direccion.setText("");
            Telefono.setText("");
        }
        else{
            em.getTransaction().commit();
            em.close();
        }

    }//GEN-LAST:event_btnModificarActionPerformed
    private void CrearModelo() {
        try {
            model = (new DefaultTableModel(
                    null, new String[]{ "Nombre", "Apellido", "Especialización", "Clinica", "Telefono", "Direccion"}) {
                Class[] types = new Class[]{
                    java.lang.String.class,                   
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,

                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            VisualizarReferimiento.setModel(model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }
    private void CargarDatos(){
        ClearTable();
         Object objeto[] = null;
         List<Referimiento> listado = ref.findReferimientoEntities();
         for (int i = 0; i < listado.size(); i++) {
             model.addRow(objeto);
             model.setValueAt(listado.get(i).getNombresMedico(), i, 0);
             model.setValueAt(listado.get(i).getApellidosMedico(), i, 1);
             model.setValueAt(listado.get(i).getEspecializacion(), i, 2);
             model.setValueAt(listado.get(i).getNombreClinica(), i, 3);
             model.setValueAt(listado.get(i).getTelefonoClinica(), i, 4);
             model.setValueAt(listado.get(i).getDireccionClinica(), i, 5);
         }
    }    public void Buscar(){
        int columnaABuscar = 0;
        if (CategoriaBusqueda.getSelectedItem().toString() == "Nombre")
                columnaABuscar = 0;
        if (CategoriaBusqueda.getSelectedItem().toString() == "Apellido")
                columnaABuscar = 1;
        if (CategoriaBusqueda.getSelectedItem().toString() == "Clinica")
                columnaABuscar = 3;
        if (CategoriaBusqueda.getSelectedItem().toString() == "Telefono")
                columnaABuscar = 4;
        trsFiltro.setRowFilter (RowFilter.regexFilter(txtBusqueda.getText(), columnaABuscar));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CategoriaBusqueda;
    private javax.swing.JTextField Direccion;
    private javax.swing.JPanel PanelBusqueda;
    private javax.swing.JPanel PanelIngreso;
    private javax.swing.JTextField Telefono;
    private javax.swing.JTable VisualizarReferimiento;
    private javax.swing.JTextField aMedico;
    private javax.swing.JButton btnModificar;
    private javax.swing.JTextField especializacion;
    private javax.swing.JLabel jApellido;
    private javax.swing.JLabel jApellido1;
    private javax.swing.JLabel jApellido2;
    private javax.swing.JLabel jApellido3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jNombre;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel jTelefono;
    private javax.swing.JTextField nClinica;
    private javax.swing.JTextField nMedico;
    private javax.swing.JTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
