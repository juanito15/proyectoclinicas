/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Referimiento;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author OsLo
 */
@Entity
@Table(name = "referimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Referimiento.findAll", query = "SELECT r FROM Referimiento r")
    , @NamedQuery(name = "Referimiento.findById", query = "SELECT r FROM Referimiento r WHERE r.id = :id")
    , @NamedQuery(name = "Referimiento.findId", query = "SELECT r FROM Referimiento r WHERE r.nombresMedico = :nombresMedico AND r.apellidosMedico = :apellidosMedico AND r.especializacion = :especializacion AND r.nombreClinica = :nombreClinica AND r.telefonoClinica = :telefonoClinica AND r.direccionClinica = :direccionClinica")
    , @NamedQuery(name = "Referimiento.Update", query = "UPDATE Referimiento r SET r.nombresMedico = :nombresMedico, r.apellidosMedico = :apellidosMedico, r.especializacion = :especializacion, r.nombreClinica = :nombreClinica, r.telefonoClinica = :telefonoClinica, r.direccionClinica = :direccionClinica WHERE r.id = :id")})
public class Referimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Lob
    @Column(name = "nombresMedico")
    private String nombresMedico;
    @Basic(optional = false)
    @Lob
    @Column(name = "apellidosMedico")
    private String apellidosMedico;
    @Basic(optional = false)
    @Lob
    @Column(name = "especializacion")
    private String especializacion;
    @Basic(optional = false)
    @Lob
    @Column(name = "nombreClinica")
    private String nombreClinica;
    @Basic(optional = false)
    @Lob
    @Column(name = "direccionClinica")
    private String direccionClinica;
    @Basic(optional = false)
    @Lob
    @Column(name = "telefonoClinica")
    private String telefonoClinica;

    public Referimiento() {
    }

    public Referimiento(Integer id) {
        this.id = id;
    }

    public Referimiento(Integer id, String nombresMedico, String apellidosMedico, String especializacion, String nombreClinica, String direccionClinica, String telefonoClinica) {
        this.id = id;
        this.nombresMedico = nombresMedico;
        this.apellidosMedico = apellidosMedico;
        this.especializacion = especializacion;
        this.nombreClinica = nombreClinica;
        this.direccionClinica = direccionClinica;
        this.telefonoClinica = telefonoClinica;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombresMedico() {
        return nombresMedico;
    }

    public void setNombresMedico(String nombresMedico) {
        this.nombresMedico = nombresMedico;
    }

    public String getApellidosMedico() {
        return apellidosMedico;
    }

    public void setApellidosMedico(String apellidosMedico) {
        this.apellidosMedico = apellidosMedico;
    }

    public String getEspecializacion() {
        return especializacion;
    }

    public void setEspecializacion(String especializacion) {
        this.especializacion = especializacion;
    }

    public String getNombreClinica() {
        return nombreClinica;
    }

    public void setNombreClinica(String nombreClinica) {
        this.nombreClinica = nombreClinica;
    }

    public String getDireccionClinica() {
        return direccionClinica;
    }

    public void setDireccionClinica(String direccionClinica) {
        this.direccionClinica = direccionClinica;
    }

    public String getTelefonoClinica() {
        return telefonoClinica;
    }

    public void setTelefonoClinica(String telefonoClinica) {
        this.telefonoClinica = telefonoClinica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Referimiento)) {
            return false;
        }
        Referimiento other = (Referimiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Referimiento.Referimiento[ id=" + id + " ]";
    }
    
}
