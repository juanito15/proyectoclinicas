/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;
import Pacientes.IngresoPaciente;
import static Pacientes.ModificarPacientes.darFormato;
import Pacientes.Pacientes;
import Pacientes.PacientesJpaController;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author OsLo
 */
public class IngresoInventario extends javax.swing.JPanel {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("ClinicasPU");
    InventarioJpaController InventarioM = new InventarioJpaController(emf);
    DefaultTableModel model;    
    private TableRowSorter trsFiltro;
    /**
     * Creates new form IngresoInventario
     */
    
    public IngresoInventario() {
        initComponents();
        buttonGroup1.add(InventarioInsumo);
        buttonGroup1.add(InventarioVenta);
        //Labels
        jNombre.setVisible(false);
        jCodigo.setVisible(false);
        jLaboratorio.setVisible(false);
        jPresentacion.setVisible(false);
        jDescripcion.setVisible(false);
        jVenta.setVisible(false);
        jCompra.setVisible(false);
        jCantidad.setVisible(false);
        //TextField
        Nombre.setVisible(false);
        Codigo.setVisible(false);
        Laboratorio.setVisible(false);
        Presentacion.setVisible(false);
        Descripcion.setVisible(false);
        Venta.setVisible(false);
        Compra.setVisible(false);
        Cantidad.setVisible(false);
        CrearModelo();
        
    }
    private void CrearModelo() {
        try {
            model = (new DefaultTableModel(
                    null, new String[]{ "Tipo","Nombre", "Código", "Descripción", "Laboratorio", "Presentacion", "Cantidad", "Precio de Compra", "Precio de Venta"}) {
                Class[] types = new Class[]{
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false, false, false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            VisualizarProductos.setModel(model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }
    private void CargarDatosInsumo(){
        ClearTable();
         Object objeto[] = null;
         List<Inventario> listado = InventarioM.findInventarioEntities();
         for (int i = 0; i < listado.size(); i++) {
             model.addRow(objeto);
             if (listado.get(i).getTipo().equals("Insumo")) {
                model.setValueAt(listado.get(i).getTipo(), i, 0);
                model.setValueAt(listado.get(i).getNombre(), i, 1);
                model.setValueAt(listado.get(i).getCodigo(), i, 2);
                model.setValueAt(listado.get(i).getDescripcion(), i, 3);
                model.setValueAt(listado.get(i).getLaboratorio(), i, 4);
                model.setValueAt(listado.get(i).getCantidad(), i, 6);
                model.setValueAt(listado.get(i).getPresentacion(), i, 5);
                model.setValueAt(listado.get(i).getPrecioCompra(), i, 7);
                model.setValueAt(listado.get(i).getPrecioVenta(), i, 8);   
             }
         }
    }    
    private void CargarDatosVentas(){
        ClearTable();
         Object objeto[] = null;
         List<Inventario> listado = InventarioM.findInventarioEntities();
         for (int i = 0; i < listado.size(); i++) {
             model.addRow(objeto);   
           if (listado.get(i).getTipo().equals("Venta")) {
                model.setValueAt(listado.get(i).getTipo(), i, 0);
                model.setValueAt(listado.get(i).getNombre(), i, 1);
                model.setValueAt(listado.get(i).getCodigo(), i, 2);
                model.setValueAt(listado.get(i).getDescripcion(), i, 3);
                model.setValueAt(listado.get(i).getLaboratorio(), i, 4);
                model.setValueAt(listado.get(i).getCantidad(), i, 6);
                model.setValueAt(listado.get(i).getPresentacion(), i, 5);
                model.setValueAt(listado.get(i).getPrecioCompra(), i, 7);
                model.setValueAt(listado.get(i).getPrecioVenta(), i, 8);
            }
         }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        PanelBusqueda = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        CategoriaBusqueda = new javax.swing.JComboBox<>();
        txtBusqueda = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        VisualizarProductos = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        InventarioVenta = new javax.swing.JRadioButton();
        InventarioInsumo = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        jNombre = new javax.swing.JLabel();
        Nombre = new javax.swing.JTextField();
        jCodigo = new javax.swing.JLabel();
        Codigo = new javax.swing.JTextField();
        jPresentacion = new javax.swing.JLabel();
        Presentacion = new javax.swing.JTextField();
        jLaboratorio = new javax.swing.JLabel();
        Laboratorio = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        Descripcion = new javax.swing.JEditorPane();
        jDescripcion = new javax.swing.JLabel();
        jCantidad = new javax.swing.JLabel();
        jCompra = new javax.swing.JLabel();
        jVenta = new javax.swing.JLabel();
        Cantidad = new javax.swing.JTextField();
        Compra = new javax.swing.JTextField();
        Venta = new javax.swing.JTextField();
        btnIngresar = new javax.swing.JButton();

        PanelBusqueda.setBorder(javax.swing.BorderFactory.createTitledBorder("Busqueda de producto"));

        jLabel2.setText("Busqueda por: ");

        CategoriaBusqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Código" }));

        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyTyped(evt);
            }
        });

        VisualizarProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(VisualizarProductos);

        javax.swing.GroupLayout PanelBusquedaLayout = new javax.swing.GroupLayout(PanelBusqueda);
        PanelBusqueda.setLayout(PanelBusquedaLayout);
        PanelBusquedaLayout.setHorizontalGroup(
            PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBusquedaLayout.createSequentialGroup()
                .addGroup(PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelBusquedaLayout.createSequentialGroup()
                        .addGroup(PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelBusquedaLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel2)
                                .addGap(14, 14, 14)
                                .addComponent(CategoriaBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PanelBusquedaLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 817, Short.MAX_VALUE))
                .addContainerGap())
        );
        PanelBusquedaLayout.setVerticalGroup(
            PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 518, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelBusquedaLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel2))
                    .addComponent(CategoriaBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de inventario"));

        InventarioVenta.setText("Venta");
        InventarioVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InventarioVentaActionPerformed(evt);
            }
        });

        InventarioInsumo.setText("Insumos");
        InventarioInsumo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InventarioInsumoMouseClicked(evt);
            }
        });
        InventarioInsumo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InventarioInsumoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(InventarioInsumo)
                .addGap(2, 2, 2)
                .addComponent(InventarioVenta)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(InventarioInsumo)
            .addComponent(InventarioVenta)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de inventario"));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jNombre.setText("Nombre: ");
        jPanel3.add(jNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 28, -1, -1));
        jPanel3.add(Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(189, 24, 210, -1));

        jCodigo.setText("Código: ");
        jPanel3.add(jCodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 70, -1, -1));
        jPanel3.add(Codigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(189, 66, 210, -1));

        jPresentacion.setText("Presentación: ");
        jPanel3.add(jPresentacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 112, -1, -1));
        jPanel3.add(Presentacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(189, 108, 210, -1));

        jLaboratorio.setText("Laboratorio: ");
        jPanel3.add(jLaboratorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 154, -1, -1));
        jPanel3.add(Laboratorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(189, 150, 210, -1));

        jScrollPane2.setViewportView(Descripcion);

        jPanel3.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 214, 390, 148));

        jDescripcion.setText("Descripción:");
        jPanel3.add(jDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 192, -1, -1));

        jCantidad.setText("Cantidad: ");
        jPanel3.add(jCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(84, 408, -1, -1));

        jCompra.setText("Precio de compra: Q");
        jPanel3.add(jCompra, new org.netbeans.lib.awtextra.AbsoluteConstraints(84, 450, -1, -1));

        jVenta.setText("Precio de venta: Q");
        jPanel3.add(jVenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(84, 488, -1, -1));
        jPanel3.add(Cantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 404, 45, -1));
        jPanel3.add(Compra, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 446, 45, -1));
        jPanel3.add(Venta, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 484, 45, -1));

        btnIngresar.setText("Guardar");
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });
        jPanel3.add(btnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 542, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PanelBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(PanelBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(101, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    public void ClearTable() {
        for (int i = 0; i < VisualizarProductos.getRowCount(); i++) {
            model.removeRow(i);
            i -= 1;
        }
    }
    private void InventarioVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InventarioVentaActionPerformed
        //Labels
        jNombre.setVisible(true);
        jCodigo.setVisible(true);
        jLaboratorio.setVisible(true);
        jPresentacion.setVisible(true);
        jDescripcion.setVisible(true);
        jVenta.setVisible(true);
        jCompra.setVisible(true);
        jCantidad.setVisible(true);
        //TextField
        Nombre.setVisible(true);
        Codigo.setVisible(true);
        Laboratorio.setVisible(true);
        Presentacion.setVisible(true);
        Descripcion.setVisible(true);
        Venta.setVisible(true);
        Compra.setVisible(true);
        Cantidad.setVisible(true);
        CargarDatosVentas();
    }//GEN-LAST:event_InventarioVentaActionPerformed

    private void InventarioInsumoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InventarioInsumoActionPerformed
        // TODO add your handling code here:
                //Labels
        jNombre.setVisible(true);
        jCodigo.setVisible(true);
        jLaboratorio.setVisible(true);
        jPresentacion.setVisible(true);
        jDescripcion.setVisible(true);
        jVenta.setVisible(false);
        jCompra.setVisible(false);
        jCantidad.setVisible(true);
        //TextField
        Nombre.setVisible(true);
        Codigo.setVisible(true);
        Laboratorio.setVisible(true);
        Presentacion.setVisible(true);
        Descripcion.setVisible(true);
        Venta.setVisible(false);
        Compra.setVisible(false);
        Cantidad.setVisible(true);
        CargarDatosInsumo();
    }//GEN-LAST:event_InventarioInsumoActionPerformed

    public void InsertarDatos(){
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        Date fechaDate = null;
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ClinicasPU");
        InventarioJpaController producto = new InventarioJpaController(emf);
        EntityManager em = emf.createEntityManager();
        Inventario ingresoInventario = new Inventario();
        String tipoInventario = "";
        if(InventarioInsumo.isSelected()){
            tipoInventario = "Insumo";
                int respuesta = JOptionPane.showConfirmDialog(null, "¿Esta seguro de guardar?", "Guardar", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                em.getTransaction().begin();
                if (respuesta == 0) {
                    ingresoInventario.setTipo(tipoInventario);
                    ingresoInventario.setNombre(Nombre.getText());
                    ingresoInventario.setCodigo(Codigo.getText());
                    ingresoInventario.setDescripcion(Descripcion.getText());
                    ingresoInventario.setLaboratorio(Laboratorio.getText());
                    ingresoInventario.setCantidad(Integer.parseInt(Cantidad.getText()));
                    ingresoInventario.setPresentacion(Presentacion.getText());              
                    em.persist(ingresoInventario);
                    em.getTransaction().commit();
                    em.close();
                    JOptionPane.showConfirmDialog(null, "¡El paciente se ha guardado correctamente!","¡EXITO!" ,JOptionPane.DEFAULT_OPTION,JOptionPane.DEFAULT_OPTION );
                    CargarDatosInsumo();
                    Nombre.setText("");
                    Codigo.setText("");
                    Descripcion.setText("");
                    Laboratorio.setText("");
                    Cantidad.setText("");
                    Presentacion.setText("");
                    Cantidad.setText("");
                }
                else{
                    em.getTransaction().commit();
                    em.close();
                }
        }
        else{
            tipoInventario = "Venta";
                int respuesta = JOptionPane.showConfirmDialog(null, "¿Esta seguro de guardar?", "Guardar", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                em.getTransaction().begin();
                if (respuesta == 0) {
                    ingresoInventario.setTipo(tipoInventario);
                    ingresoInventario.setNombre(Nombre.getText());
                    ingresoInventario.setCodigo(Codigo.getText());
                    ingresoInventario.setDescripcion(Descripcion.getText());
                    ingresoInventario.setLaboratorio(Laboratorio.getText());
                    ingresoInventario.setCantidad(Integer.parseInt(Cantidad.getText()));
                    ingresoInventario.setPresentacion(Presentacion.getText());                   
                    ingresoInventario.setPrecioCompra(Float.parseFloat(Compra.getText()));
                    ingresoInventario.setPrecioVenta(Float.parseFloat(Venta.getText()));            
                    em.persist(ingresoInventario);
                    em.getTransaction().commit();
                    em.close();
                    JOptionPane.showConfirmDialog(null, "¡El paciente se ha guardado correctamente!","¡EXITO!" ,JOptionPane.DEFAULT_OPTION,JOptionPane.DEFAULT_OPTION );
                    CargarDatosVentas();
                    Nombre.setText("");
                    Codigo.setText("");
                    Descripcion.setText("");
                    Laboratorio.setText("");
                    Cantidad.setText("");
                    Presentacion.setText("");
                    Compra.setText("");
                    Cantidad.setText("");
                    Venta.setText("");
                }
                else{
                    em.getTransaction().commit();
                    em.close();
                }
        }
    }
    
    private void InventarioInsumoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InventarioInsumoMouseClicked

    }//GEN-LAST:event_InventarioInsumoMouseClicked

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
        // TODO add your handling code here:
        InsertarDatos();
    }//GEN-LAST:event_btnIngresarActionPerformed
    public void BuscarProducto(){
        int columnaABuscar = 0;
        if (CategoriaBusqueda.getSelectedItem().toString() == "Nombre")
                columnaABuscar = 1;
        if (CategoriaBusqueda.getSelectedItem().toString() == "Código")
                columnaABuscar = 2;
        trsFiltro.setRowFilter (RowFilter.regexFilter(txtBusqueda.getText(), columnaABuscar));
    }
    private void txtBusquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyTyped
        // TODO add your handling code here:
        txtBusqueda.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (txtBusqueda.getText());
                txtBusqueda.setText(cadena);
                repaint();
                BuscarProducto();
            }
        });
        trsFiltro = new TableRowSorter(VisualizarProductos.getModel());
        VisualizarProductos.setRowSorter(trsFiltro);
    }//GEN-LAST:event_txtBusquedaKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Cantidad;
    private javax.swing.JComboBox<String> CategoriaBusqueda;
    private javax.swing.JTextField Codigo;
    private javax.swing.JComboBox<String> ComboLetra;
    private javax.swing.JComboBox<String> ComboNivel;
    private javax.swing.JTextField Compra;
    private javax.swing.JEditorPane Descripcion;
    private javax.swing.JRadioButton InventarioInsumo;
    private javax.swing.JRadioButton InventarioVenta;
    private javax.swing.JTextField Laboratorio;
    private javax.swing.JTextField Nombre;
    private javax.swing.JPanel PanelBusqueda;
    private javax.swing.JTextField Presentacion;
    private javax.swing.JTextField Venta;
    private javax.swing.JTable VisualizarProductos;
    private javax.swing.JButton btnIngresar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jCantidad;
    private javax.swing.JLabel jCodigo;
    private javax.swing.JLabel jCompra;
    private javax.swing.JLabel jDescripcion;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLaboratorio;
    private javax.swing.JLabel jNombre;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel jPresentacion;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel jVenta;
    private javax.swing.JTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
