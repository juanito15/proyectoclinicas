/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author OsLo
 */
@Entity
@Table(name = "inventario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventario.findAll", query = "SELECT i FROM Inventario i")
    , @NamedQuery(name = "Inventario.findById", query = "SELECT i FROM Inventario i WHERE i.id = :id")
    , @NamedQuery(name = "Inventario.findByTipo", query = "SELECT i FROM Inventario i WHERE i.tipo = :tipo")
    , @NamedQuery(name = "Inventario.findByCantidad", query = "SELECT i FROM Inventario i WHERE i.cantidad = :cantidad")
    , @NamedQuery(name = "Inventario.findByPrecioVenta", query = "SELECT i FROM Inventario i WHERE i.precioVenta = :precioVenta")
    , @NamedQuery(name = "Inventario.findByPrecioCompra", query = "SELECT i FROM Inventario i WHERE i.precioCompra = :precioCompra")
    , @NamedQuery(name = "Inventario.findID", query = "SELECT i FROM Inventario i where i.tipo = :tipo AND i.nombre = :nombre AND i.codigo = :codigo AND i.descripcion = :descripcion AND i.laboratorio = :laboratorio AND i.cantidad = :cantidad AND i.presentacion = :presentacion AND i.precioCompra = :precioCompra AND i.precioVenta = :precioVenta")
    , @NamedQuery(name = "Inventario.findID1", query = "SELECT i FROM Inventario i where i.tipo = :tipo AND i.nombre = :nombre AND i.codigo = :codigo AND i.descripcion = :descripcion AND i.laboratorio = :laboratorio AND i.cantidad = :cantidad AND i.presentacion = :presentacion")
    , @NamedQuery(name = "Inventario.Update", query = "UPDATE Inventario i SET i.tipo = :tipo, i.nombre = :nombre, i.codigo = :codigo, i.descripcion = :descripcion, i.laboratorio = :laboratorio, i.cantidad = :cantidad, i.presentacion = :presentacion WHERE i.id = :id")
    , @NamedQuery(name = "Inventario.Update1", query = "UPDATE Inventario i SET i.tipo = :tipo, i.nombre = :nombre, i.codigo = :codigo, i.descripcion = :descripcion, i.laboratorio = :laboratorio, i.cantidad = :cantidad, i.presentacion = :presentacion, i.precioCompra = :precioCompra, i.precioVenta = :precioVenta WHERE i.id = :id")})

public class Inventario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "tipo")
    private String tipo;
    @Basic(optional = false)
    @Lob
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Lob
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @Lob
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Lob
    @Column(name = "laboratorio")
    private String laboratorio;
    @Basic(optional = false)
    @Column(name = "cantidad")
    private int cantidad;
    @Basic(optional = false)
    @Lob
    @Column(name = "presentacion")
    private String presentacion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precioVenta")
    private Float precioVenta;
    @Column(name = "precioCompra")
    private Float precioCompra;

    public Inventario() {
    }

    public Inventario(Integer id) {
        this.id = id;
    }

    public Inventario(Integer id, String tipo, String nombre, String codigo, String descripcion, String laboratorio, int cantidad, String presentacion) {
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.laboratorio = laboratorio;
        this.cantidad = cantidad;
        this.presentacion = presentacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public Float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(Float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(Float precioCompra) {
        this.precioCompra = precioCompra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventario)) {
            return false;
        }
        Inventario other = (Inventario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Inventario.Inventario[ id=" + id + " ]";
    }
    
}
