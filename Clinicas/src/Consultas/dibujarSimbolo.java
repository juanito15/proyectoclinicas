/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Consultas;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 *
 * @author juanm
 */
public class dibujarSimbolo extends JComponent{
    private ImageIcon icono;
    //private int x , y;
    public dibujarSimbolo(ImageIcon icono, Dimension size) {
        this.icono = icono;
        //this.x = x;
        //this.y = y;
        super.setSize(size);
    }
    
    
    @Override
    public void paint(Graphics g){
        super.paint(g);
        g.drawImage(icono.getImage(), 0 , 0, null);
    }
}
