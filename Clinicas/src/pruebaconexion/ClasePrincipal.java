/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaconexion;
import Pacientes.IngresoPaciente;
import Usuarios.IngresarUsuario;
import Usuarios.Login;
import Usuarios.Usuarios;
import Usuarios.UsuariosJpaController;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author OsLo
 */
public class ClasePrincipal {

    public static void main(String args[]) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ClinicasPU");
        UsuariosJpaController user = new UsuariosJpaController(emf);
        EntityManager em = emf.createEntityManager();
        Query query = em.createNamedQuery("Usuarios.CountID", Usuarios.class);
        long count = (long)query.getSingleResult();
        IngresarUsuario IU = new IngresarUsuario();
        Login login = new Login();
        if (count == 0)            
            IU.setVisible(true);
        else
            login.show(true);        
    }

}
