/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import ConsultaGeneral.FormGeneral;
import ConsultaGeneral.General;
import Interfaces.Consultas;

/**
 *
 * @author OsLo
 */
public class ClaseIntermedia implements Consultas {
    String nombre = "", apellido = "", telefono = "", alergias = "";
    int idPacientes = 0;
    FormGeneral g = new FormGeneral();
    
    public void setID(int id){
        idPacientes = id;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setApellido(String apellido){
        this.apellido = apellido;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getAlergias() {
        return alergias;
    }

    public int getIdPacientes() {
        return idPacientes;
    }

    @Override
    public void idPaciente(int id) {
        g.setID(id);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void nombre(String nombre) {
    g.setNombre(nombre);    
    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void apellido(String apellido) {
        g.setApellido(apellido);        
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void telefono(String telefono) {
        g.setTelefono(telefono);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void alergias(String alergias) {
        g.setAlergias(alergias);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
