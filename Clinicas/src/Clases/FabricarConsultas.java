/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
import Interfaces.Consultas;
import Interfaces.ConsultasConcretas;

/**
 *
 * @author OsLo
 */
public class FabricarConsultas {
    	public static void crearFabricaConsultas(ConsultasConcretas factory, int id, String nombre, String apellido, String telefono, String alergia)
	{
		Consultas objetoConsulta= factory.crearConsulta();
		objetoConsulta.idPaciente(id);
                objetoConsulta.nombre(nombre);
                objetoConsulta.apellido(apellido);
                objetoConsulta.telefono(telefono);
                objetoConsulta.alergias(alergia);
	}
}
