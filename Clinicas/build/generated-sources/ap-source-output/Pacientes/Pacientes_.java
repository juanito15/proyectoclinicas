package Pacientes;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-07-08T17:28:43")
@StaticMetamodel(Pacientes.class)
public class Pacientes_ { 

    public static volatile SingularAttribute<Pacientes, String> apellidos;
    public static volatile SingularAttribute<Pacientes, Date> fechaNacimiento;
    public static volatile SingularAttribute<Pacientes, String> tratamientos;
    public static volatile SingularAttribute<Pacientes, String> genero;
    public static volatile SingularAttribute<Pacientes, Integer> id;
    public static volatile SingularAttribute<Pacientes, String> telefono;
    public static volatile SingularAttribute<Pacientes, String> nombres;
    public static volatile SingularAttribute<Pacientes, String> alergias;

}