package Inventario;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-07-08T17:28:43")
@StaticMetamodel(Inventario.class)
public class Inventario_ { 

    public static volatile SingularAttribute<Inventario, String> descripcion;
    public static volatile SingularAttribute<Inventario, String> tipo;
    public static volatile SingularAttribute<Inventario, String> codigo;
    public static volatile SingularAttribute<Inventario, Float> precioCompra;
    public static volatile SingularAttribute<Inventario, String> presentacion;
    public static volatile SingularAttribute<Inventario, Integer> id;
    public static volatile SingularAttribute<Inventario, String> laboratorio;
    public static volatile SingularAttribute<Inventario, Integer> cantidad;
    public static volatile SingularAttribute<Inventario, Float> precioVenta;
    public static volatile SingularAttribute<Inventario, String> nombre;

}